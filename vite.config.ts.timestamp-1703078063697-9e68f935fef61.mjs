// vite.config.ts
import { defineConfig } from "file:///D:/Java%E7%BC%96%E7%A8%8B/%E5%89%8D%E7%AB%AF/syt/s1/syt/node_modules/vite/dist/node/index.js";
import vue from "file:///D:/Java%E7%BC%96%E7%A8%8B/%E5%89%8D%E7%AB%AF/syt/s1/syt/node_modules/@vitejs/plugin-vue/dist/index.mjs";
import path from "path";
var __vite_injected_original_dirname = "D:\\Java\u7F16\u7A0B\\\u524D\u7AEF\\syt\\s1\\syt";
var vite_config_default = defineConfig({
  plugins: [vue()],
  //src文件夹配置别名
  resolve: {
    alias: {
      //配置@为绝对路径src
      "@": path.resolve(__vite_injected_original_dirname, "src"),
      vue: path.resolve("./node_modules/vue")
    }
  },
  //配置代理
  server: {
    proxy: {
      "/api": {
        target: "http://192.168.1.100:8080",
        //1.100  //3.240
        changeOrigin: true
      }
    }
  }
});
export {
  vite_config_default as default
};
//# sourceMappingURL=data:application/json;base64,ewogICJ2ZXJzaW9uIjogMywKICAic291cmNlcyI6IFsidml0ZS5jb25maWcudHMiXSwKICAic291cmNlc0NvbnRlbnQiOiBbImNvbnN0IF9fdml0ZV9pbmplY3RlZF9vcmlnaW5hbF9kaXJuYW1lID0gXCJEOlxcXFxKYXZhXHU3RjE2XHU3QTBCXFxcXFx1NTI0RFx1N0FFRlxcXFxzeXRcXFxcczFcXFxcc3l0XCI7Y29uc3QgX192aXRlX2luamVjdGVkX29yaWdpbmFsX2ZpbGVuYW1lID0gXCJEOlxcXFxKYXZhXHU3RjE2XHU3QTBCXFxcXFx1NTI0RFx1N0FFRlxcXFxzeXRcXFxcczFcXFxcc3l0XFxcXHZpdGUuY29uZmlnLnRzXCI7Y29uc3QgX192aXRlX2luamVjdGVkX29yaWdpbmFsX2ltcG9ydF9tZXRhX3VybCA9IFwiZmlsZTovLy9EOi9KYXZhJUU3JUJDJTk2JUU3JUE4JThCLyVFNSU4OSU4RCVFNyVBQiVBRi9zeXQvczEvc3l0L3ZpdGUuY29uZmlnLnRzXCI7aW1wb3J0IHsgZGVmaW5lQ29uZmlnIH0gZnJvbSAndml0ZSdcbmltcG9ydCB2dWUgZnJvbSAnQHZpdGVqcy9wbHVnaW4tdnVlJ1xuXG5pbXBvcnQgcGF0aCBmcm9tICdwYXRoJztcbmV4cG9ydCBkZWZhdWx0IGRlZmluZUNvbmZpZyh7XG4gIHBsdWdpbnM6IFt2dWUoKV0sXG4gIC8vc3JjXHU2NTg3XHU0RUY2XHU1OTM5XHU5MTREXHU3RjZFXHU1MjJCXHU1NDBEXG4gIHJlc29sdmU6e1xuICAgIGFsaWFzOntcbiAgICAgIC8vXHU5MTREXHU3RjZFQFx1NEUzQVx1N0VERFx1NUJGOVx1OERFRlx1NUY4NHNyY1xuICAgICAgXCJAXCI6cGF0aC5yZXNvbHZlKF9fZGlybmFtZSwnc3JjJyksXG4gICAgICB2dWU6IHBhdGgucmVzb2x2ZShcIi4vbm9kZV9tb2R1bGVzL3Z1ZVwiKSxcbiAgICB9XG4gIH0sXG4gIC8vXHU5MTREXHU3RjZFXHU0RUUzXHU3NDA2XG4gIHNlcnZlcjp7XG4gICAgcHJveHk6e1xuICAgICAgJy9hcGknOiB7XG4gICAgICAgIHRhcmdldDogJ2h0dHA6Ly8xOTIuMTY4LjEuMTAwOjgwODAnLC8vMS4xMDAgIC8vMy4yNDBcbiAgICAgICAgY2hhbmdlT3JpZ2luOiB0cnVlLFxuICAgICAgfSxcbiAgICB9XG4gIH1cbn0pXG4iXSwKICAibWFwcGluZ3MiOiAiO0FBQW1TLFNBQVMsb0JBQW9CO0FBQ2hVLE9BQU8sU0FBUztBQUVoQixPQUFPLFVBQVU7QUFIakIsSUFBTSxtQ0FBbUM7QUFJekMsSUFBTyxzQkFBUSxhQUFhO0FBQUEsRUFDMUIsU0FBUyxDQUFDLElBQUksQ0FBQztBQUFBO0FBQUEsRUFFZixTQUFRO0FBQUEsSUFDTixPQUFNO0FBQUE7QUFBQSxNQUVKLEtBQUksS0FBSyxRQUFRLGtDQUFVLEtBQUs7QUFBQSxNQUNoQyxLQUFLLEtBQUssUUFBUSxvQkFBb0I7QUFBQSxJQUN4QztBQUFBLEVBQ0Y7QUFBQTtBQUFBLEVBRUEsUUFBTztBQUFBLElBQ0wsT0FBTTtBQUFBLE1BQ0osUUFBUTtBQUFBLFFBQ04sUUFBUTtBQUFBO0FBQUEsUUFDUixjQUFjO0FBQUEsTUFDaEI7QUFBQSxJQUNGO0FBQUEsRUFDRjtBQUNGLENBQUM7IiwKICAibmFtZXMiOiBbXQp9Cg==
