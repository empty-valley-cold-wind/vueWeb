import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

import path from 'path';
export default defineConfig({
  plugins: [vue()],
  //src文件夹配置别名
  resolve:{
    alias:{
      //配置@为绝对路径src
      "@":path.resolve(__dirname,'src'),
      vue: path.resolve("./node_modules/vue"),
    }
  },
  //配置代理
  server:{
    proxy:{
      'api': {
        target: 'http://192.168.102.8:8080',//1.100  //3.240   26.33.57.23
        changeOrigin: true,
        ws:true
      },
    }
  }
})
