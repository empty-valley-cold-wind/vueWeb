//定义用户相关仓库
import {defineStore} from "pinia";
import {UserStore} from "@/api/user/type.ts";
import {LevelDict} from "@/api/levelDict/type.ts";

const useUserStore = defineStore('User',{
    state:()=>{
        return {
            visible: false,//控制登陆组件的dialog显示与隐藏
            userInfo:JSON.parse(localStorage.getItem('USERINFO') as string) as UserStore||{} as UserStore,
            userMessage:[] as LevelDict[]
        }
    },
    actions:{

    },
    getters:{

    }
})

export default useUserStore