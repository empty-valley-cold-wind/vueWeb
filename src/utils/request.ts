//d axios进行二次封装
//利用axios请求/响应拦截器
//请求中携带公共的参数，如token
//响应简化服务器返回的数据，处理http的网络错误
import axios from "axios";
import {ElMessage} from "element-plus";

//利用给axios.crete方法创建一个实例，设置基础路径以及超时时间
const request = axios.create({
    baseURL:'http://192.168.102.8/api',//1.100  //3.240  26.33.57.23
    timeout:500000,//超过5秒请求失败
})
//请求拦截器
request.interceptors.request.use((config)=>{
    //config:请求拦截器回调注入的对象/配置对象

    return config;
})

//响应拦截器
request.interceptors.response.use((response)=>{
    return response.data
},(error)=>{
    //处理错误
    let status:number = error.response.status
    switch (status){
        case 404:
            ElMessage({
                type:'error',
                message:'请求失败，路径不存在'
            })
            break;
        case 500|501|502|503|504|505:
            ElMessage({
                type:'error',
                message:'服务器错误'
            })
            break;
        case 401:{
            ElMessage({
                type:'error',
                message:'参数有误'
            })
            break;
        }
    }
    return Promise.reject(new Error(error.message))
})


export default request