import request from "@/utils/request.ts";
import {DataAnalysisResponseData} from "@/admin/analysis/type.ts";


enum API{
    //获取分析数据
    GET_ANALYSIS = '/core/admin/analysis/getDataAnalysis',
}

export const getAnalysis = (token:string)=>
    request.get<any,DataAnalysisResponseData>(API.GET_ANALYSIS,{
        headers:{
            token:token
        }
    })