

export interface ResponseData{
    code:number,
    message:string,
}

export interface DataAnalysis{
    //过去24小时访问量
    lastVisit:number,
    //过去24小时注册量
    lastRegister:number,
    //过去24小时提交量
   lastPosting:number,
    //未审核帖子数量
    notAccessVetting:number,
    //已通过审核数量
    accessVetting:number,
}

export interface DataAnalysisResponseData extends ResponseData{
    data:{
        dataAnalysis:DataAnalysis
    }
}