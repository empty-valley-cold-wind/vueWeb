export interface ResponseData{
    code:number,
    message:string,
}

export interface Part{
    partId:number,
    parentId:number,
    partName:string,
    value:number
}

export interface PartDetail extends Part{
    children:Part[]
}
export interface PartDetailListResponseData extends ResponseData{
    data:{
        partList:PartDetail[]
    }
}