import request from "@/utils/request.ts";
import {Part, PartDetailListResponseData, ResponseData} from "@/admin/part/type.ts";

enum API{
    //获取分区列表
    GET_PART_LIST='/core/admin/part/getAllPart',
    //更新分区名
    UPDATE_PART_NAME='/core/admin/part/updatePartName/',

    ADD_PART='/core/admin/part/addPart',
    DELETE_PART='/core/admin/part/deletePart/'
}

export const getPartList = (token:string)=>
    request.get<any,PartDetailListResponseData>(API.GET_PART_LIST,{
        headers:{
            token:token
        }
    })

export const updatePartName = (partId:number,partName:string,reason:string,token:string)=>
    request.post<any,ResponseData>(API.UPDATE_PART_NAME+`${partId}?reason=${reason}&partName=${partName}`,'',{
        headers:{
            token:token
        }
    })
export const savePart = (part:Part,reason:string,token:string)=>
    request.post<any,ResponseData>(API.ADD_PART+`?reason=${reason}`,part,{
        headers:{
            token:token
        }
    })

export const deletePart = (partId:number,reason:string,token:string)=>
    request.post<any,ResponseData>(API.DELETE_PART+`${partId}?reason=${reason}`,'',{
        headers:{
            token:token
        }
    })
