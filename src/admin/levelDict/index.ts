import request from "@/utils/request.ts";
import {ResponseData} from "@/api/home/type.ts";
import {LevelDict} from "@/api/levelDict/type.ts";

enum API{
    //移除等级
    DELETE_LEVEL='/core/admin/level/deleteLevel/',
    //添加等级
    ADD_LEVEL='/core/admin/level/addLevel/',
    //修改等级
    UPDATE_LEVEL='/core/admin/level/updateLevel/',
}

export const deleteLevel = (levelId:number,reason:string,token:string)=>
    request.post<any,ResponseData>(API.DELETE_LEVEL+`${levelId}/?reason=${reason}`,'',{
        headers:{
            token:token
        }
    })
export const addLevel = (levelDict:LevelDict,reason:string,token:string)=>
    request.post<any,ResponseData>(API.ADD_LEVEL+`?reason=${reason}`,levelDict,{
        headers:{
            token:token
        }
    })
export const updateLevel = (levelDict:LevelDict,reason:string,token:string)=>
    request.post<any,ResponseData>(API.UPDATE_LEVEL+`?reason=${reason}`,levelDict,{
        headers:{
            token:token
        }
    })