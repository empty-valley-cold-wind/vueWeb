export interface ResponseData{
    code:number,
    message:string,
}
export interface Event{
    eventId:number,
    eventType:number,
    eventMessage:string,
    arId:string,
    userId:number,
    commentId:string,
    isHandle:number,
    result:string,
    eventTime:string,
    handleTime:string
}
export interface EventDetail{
    event:Event,
    pageNo:number,
    scroll:number,
    username:string
}
export interface GetEventListResponseData extends ResponseData{
    data:{
        eventPage:{
            records:EventDetail[],
            total:number
        }

    }
}