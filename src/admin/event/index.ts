import request from "@/utils/request.ts";
import {GetEventListResponseData} from "@/admin/event/type.ts";
import {ResponseData} from "@/api/home/type.ts";

enum API{
    //获取事件列表
    GET_EVENT_LIST='/core/admin/event/getEventList/',
    //处理事件
    HANDLE_EVENT='/core/admin/event/handleEvent/'
}

export const getEventList =(pageNo:number,pageSize:number,token:string)=> request.get<any,GetEventListResponseData>(API.GET_EVENT_LIST+`${pageNo}/${pageSize}`,{
    headers:{
        token:token
    }
})
export const handleEvent =(eventId:number,result:string,token:string)=> request.post<any,ResponseData>(API.HANDLE_EVENT+`${eventId}/?result=${result}`,'',{
    headers:{
        token:token
    }
})