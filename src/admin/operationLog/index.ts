import request from "@/utils/request.ts";
import {GetOperationLogListResponseData} from "@/admin/operationLog/type.ts";

enum API {
    GET_OPERATION_LIST='/core/admin/operation/getOperationLogList/'
}

export const getOperationLogList = (pageNo:number,pageSize:number,token:string)=>
    request.get<any,GetOperationLogListResponseData>(API.GET_OPERATION_LIST+`${pageNo}/${pageSize}`,{
        headers:{
            token:token
        }
    })