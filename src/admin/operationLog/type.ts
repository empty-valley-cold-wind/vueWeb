export interface ResponseData{
    code:number,
    message:string,
}

export interface OperationLog{
    logId:number,
    operationMessage:string,
    reason:string,
    userId:number,
    username:string,
    ip:string,
    operationTime:string
}
export interface GetOperationLogListResponseData extends ResponseData{
    data:{
        operationList:{
            records:OperationLog[],
            current:number,
            size:number,
            total:number,
        }
    }
}
