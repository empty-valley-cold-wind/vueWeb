import request from "@/utils/request.ts";
import {ArticlesResponseData} from "@/admin/article/type.ts";
import {ResponseData} from "@/admin/user/type.ts";

enum API{
    GET_NOT_VETTING_ARTICLES='/core/admin/article/getArticleListByVet/',
    DO_VETTING='/core/admin/article/doVetting/',
    DELETE_ARTICLE='/core/admin/article/deleteArticle/',
    UPDATE_ARTICLE_LEVEL='/core/admin/article/updateArticleLevel/',
    UPDATE_ARTICLE_RECOMMEND='/core/admin/article/updateArticleRecommend/'
}

export const getArticleListByVet = (pageNo:number,pageSize:number,vet:number,parentId:number,partId:number,level:number,isReprint:number,query:string,startDate:number,endDate:number,token:string)=>
    request.get<any,ArticlesResponseData>(API.GET_NOT_VETTING_ARTICLES+`${pageNo}/${pageSize}/${vet}/${parentId}/${partId}/${level}/${isReprint}/${startDate}/${endDate}?query=${query}`,{
        headers:{
            token:token
        }
    })
export const doVetting = (arId:number,vet:number,reason:string,token:string)=>
    request.post<any,ResponseData>(API.DO_VETTING+`${arId}/${vet}?reason=${reason}`,'',{
        headers:{
            token:token
        }
    })
export const deleteArticle = (arId:number,reason:string,token:string)=>
    request.post<any,ResponseData>(API.DELETE_ARTICLE+`${arId}?reason=${reason}`,'',{
        headers:{
            token:token
        }
    })
export const updateArticleLevel = (arId:number,level:number,reason:string,token:string)=>
    request.post<any,ResponseData>(API.UPDATE_ARTICLE_LEVEL+`${arId}/${level}?reason=${reason}`,'',{
        headers:{
            token:token
        }
    })
export const updateArticleRecommend = (arId:number,recommend:number,reason:string,token:string)=>
    request.post<any,ResponseData>(API.UPDATE_ARTICLE_RECOMMEND+`${arId}/${recommend}?reason=${reason}`,'',{
        headers:{
            token:token
        }
    })