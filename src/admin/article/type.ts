

export interface ResponseData{
    code:number,
    message:string,
}
export interface Article{
    arId:string
    partId:number,
    parentId:number,
    userId:number,
    username:string,
    title:string,
    content:string,
    level:number,
    createTime:string,
    updateTime:string,
    likeNum:number,//点赞数
    comNum:number,//评论数
    click:number,
    coverAddress:string,//封面地址,
    description:string,//简要描述
    isReprint:number,//是否转载
    originalAddress:string,//原帖地址
    isVetting:number//审核状态
    isRecommend:number//推荐状态
}
export interface ArticleDetail extends Article{
    photoAddress:string,
    like:boolean,
    jurisdiction:number,
    partName:string,
    partChildName:string
}
export interface ArticlesResponseData extends ResponseData{
    data:{
        articles:{
            records:ArticleDetail[],
            current:number,
            size:number,
            total:number,
        }
    }
}