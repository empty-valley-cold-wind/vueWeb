export interface ResponseData{
    code:number,
    message:string,

}
export interface UserInfo{
    userId:number,
    username:string,
    idiograph:string,//用户签名
    photoAddress:string,//头像地址
    phone:string,
    jurisdiction:number,//权限,
    experience:number,
    createTime:string,
    manage:number,
    email:string,
    gold:number,//金锭数
    diamond:number,//钻石数
    isLock:number,//是否被u锁定
}
export interface UserList{
    userList:UserInfo[]
}
export interface UpdateUserResponseData extends ResponseData{
    data:{
        user:UserInfo
    }
}

export interface UserListResponseData extends ResponseData{
    data:{
        userList:{
            records:UserInfo[],
            current:number,
            size:number,
            total:number,
        }
    }
}