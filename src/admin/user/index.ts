import request from "@/utils/request.ts";
import {ResponseData, UpdateUserResponseData, UserInfo, UserListResponseData} from "@/admin/user/type.ts";

enum API{
    //获取用户列表
    GET_USER_LIST='/core/admin/user/getUserList/',
    //
    UPDATE_USER='/core/admin/user/updateUser',
    //删除用户
    DELETE_USER='/core/admin/user/deleteUser/',
    //锁定/解锁用户
    LOCK_USER='/core/admin/user/lockUser/',
    //设置用户为版主
    CHANGE_MANAGE='/core/admin/user/changeManage/',

}

export const getUserList = (pageNo:number,pageSize:number,sortFlag:number,order:number,search:string,token:string)=>
    request.get<any,UserListResponseData>(API.GET_USER_LIST+`${pageNo}/${pageSize}/${sortFlag}/${order}?search=${search}`,{
        headers:{
            token:token
        }
    })

export const updateUser = (user:UserInfo,reason:string,token:string)=>
    request.post<any,UpdateUserResponseData>(API.UPDATE_USER+`?reason=${reason}`,user,{
        headers:{
            token:token
        }
    })

export const deleteUser = (userId:number,reason:string,token:string)=>
    request.post<any,ResponseData>(API.DELETE_USER+`${userId}?reason=${reason}`,'',{
        headers:{
            token:token
        }
    })

export const changeLockUser = (userId:number,reason:string,token:string)=>
    request.post<any,ResponseData>(API.LOCK_USER+`${userId}?reason=${reason}`,'',{
        headers:{
            token:token
        }
    })

export const changeManage = (userId:number,partId:number,reason:string,token:string)=>
    request.post<any,ResponseData>(API.CHANGE_MANAGE+`${userId}/${partId}?reason=${reason}`,'',{
        headers:{
            token:token
        }
    })