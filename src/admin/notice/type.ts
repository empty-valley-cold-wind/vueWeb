export interface ResponseData{
    code:number,
    message:string,
}
export interface Notice{
    noticeId:number,
    noticeMessage:string,
    noticeTime:string,
    broadcastId:string,

}
export interface BroadResponseData extends ResponseData{
    data:{
        noticeList:{
            records:Notice[],
            current:number,
            size:number,
            total:number,
        }
    }
}