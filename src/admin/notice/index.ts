import request from "@/utils/request.ts";
import {BroadResponseData, ResponseData} from "@/admin/notice/type.ts";

enum API{
    PUBLISH_NOTICE='/core/admin/notice/publishNotice',
    GET_BROAD_NOTICE='/core/admin/notice/getBroadcast/',
    DELETE_BROADCAST = '/core/admin/notice/deleteBroadCast/'
}

export const publishNotice = (message:string,reason:string,token:string)=>
    request.post<any,ResponseData>(API.PUBLISH_NOTICE+`?message=${message}&reason=${reason}`,'',{
        headers:{
            token:token
        }
    })
export const getBroadcast = (pageNo:number,pageSize:number,token:string)=>
    request.get<any,BroadResponseData>(API.GET_BROAD_NOTICE+`${pageNo}/${pageSize}`,{
        headers:{
            token:token
        }
    })
export const deleteBroadcast = (broadcastId:string,reason:string,token:string)=>
    request.post<any,ResponseData>(API.DELETE_BROADCAST+`${broadcastId}?reason=${reason}`,'',{
        headers:{
            token:token
        }
    })