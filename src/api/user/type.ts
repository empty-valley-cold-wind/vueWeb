export interface ResponseData{
    code:number,
    message:string,

}
//用户注册接口的参数
export interface RegisterData{
    phone:string,
    password:string,
    username:string,
    code:string
}
export interface UserInfo{
    userId:number,
    username:string,
    idiograph:string,//用户签名
    photoAddress:string,//头像地址
    phone:string,
    manage:number,
    jurisdiction:number,//权限,
    experience:number,
    gold:number,//金锭数
    diamond:number,//钻石数
    isLock:number,//是否被u锁定
}
//用户登陆接口返回的参数
export interface LoginData{
    userId:number,
    token:string,//用户验证
    username:string,
    idiograph:string,//用户签名
    photoAddress:string,//头像地址
    phone:string,
    jurisdiction:number,//权限,
    experience:number,
    gold:number,//金锭数
    diamond:number,//钻石数
    isLock:number,//是否被u锁定
}
export interface UserStore{
    userId:number,
    token:string
}
export interface Collection{
    arId:string,
    userId:number,
    username:string,
    title:string,
    direction:string
}
export interface CollectionVo{
    direction:string,
    collections:Collection[]
}
export interface GetDirectionListResponseData extends ResponseData{
    data:{
        directionList:string[]
    }
}
export interface GetCollectionResponseData extends ResponseData{
    data:{
        collectionList:CollectionVo[]
    }
}
export interface LoginResponseData extends ResponseData{
    data: { userInfo:LoginData }
}

export interface CsrfResponseData extends ResponseData{
    data: { csrfToken:string }
}

export interface UserInfoResponseData extends ResponseData{
    data: { userInfo:UserInfo }
}