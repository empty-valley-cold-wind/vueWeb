//引入二次封装的ts
import request from '@/utils/request.ts'
import {
    CsrfResponseData, GetCollectionResponseData,
    LoginResponseData,
    RegisterData,
    ResponseData, UserInfo,
    UserInfoResponseData
} from "@/api/user/type.ts";

//枚举请求地址
  enum API{
      //注册
      REGISTER_URL = '/core/user/register/',
      //根据密码登陆
      LOGIN_P_URL = '/core/user/loginByPassword/',
      //根据验证码登陆
      LOGIN_C_URL = '/core/user/loginByCode/',
      //获取验证码
      GET_CODE_URL = '/sms/send/',
      //检查token
      CHECK_TOKEN = '/core/user/checkToken',
      //更新用户信息
      UPDATE_USERINFO='/core/user/updateUserInfo',
      //更新用户密码
      UPDATE_PASSWORD='/core/user/updatePassword/',
      //验证验证码
      VERIFY_CODE='/core/user/verifyCode/',
      //更新手机
      UPDATE_NEW_PHONE='/core/user/updateNewPhone/',
      //获取用户信息
      GET_USERINFO='/core/user/getUserInfo',
      //获取收藏列表
      GET_COLLECTION_LIST='/core/collection/getCollection/',

  }
//注册接口
  export const registerUser = (data:RegisterData)=>request.post<any,ResponseData>(API.REGISTER_URL,data)
//登陆接口
export const loginByPass = (data:any)=>request.post<any,LoginResponseData>(API.LOGIN_P_URL,data)
export const loginByC = (data:any)=>request.post<any,LoginResponseData>(API.LOGIN_C_URL,data)

export const getCode = (data:string,flag:number)=>request.get<any,ResponseData>(API.GET_CODE_URL+`${data}/${flag}`)

export const checkToken = (token:string)=>request.get<any,ResponseData>(API.CHECK_TOKEN,{
    headers:{
        token:token
    }
})
//更新用户信息
export const updateUserInfo = (userInfo:UserInfo,token:string)=>request.post<any,LoginResponseData>(API.UPDATE_USERINFO,userInfo,{
    headers:{
        token:token
    }
})
//验证验证码
export const verifyCode = (phone:string,code:string)=>request.get<any,CsrfResponseData>(API.VERIFY_CODE+`${code}/${phone}`)
//更新密码
export const updatePassword = (phone:string,password:string,token:string,csrfToken:string)=>request.post<any,ResponseData>(API.UPDATE_PASSWORD+`${phone}/${password}`,{},{
    headers:{
        token:token,
        csrfToken:csrfToken
    }
})
//更新手机号
export const updateNewPhone = (phone:string,newPhone:string,code:string,token:string,csrfToken:string)=>request.post<any,ResponseData>(API.UPDATE_NEW_PHONE+`${phone}/${newPhone}/${code}`,{},{
    headers:{
        token:token,
        csrfToken:csrfToken
    }
})

//获取用户信息
export const getUserInfo = (token:string)=>request.get<any,UserInfoResponseData>(API.GET_USERINFO,{
    headers:{
        token:token
    }
})

export const getCollections = (token:string)=>
    request.get<any,GetCollectionResponseData>(API.GET_COLLECTION_LIST,{
        headers:{
            token:token
        }
    })