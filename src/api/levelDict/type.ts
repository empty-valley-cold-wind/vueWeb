export interface ResponseData{
    code:number,
    message:string,
}

export interface LevelDict{
    levelId:number,
    experience:number,
    levelName:string,
    levelNumber:number,
    isComment:number,
    isArticle:number,
    isReward:number
}
export interface LevelDictListResponseData extends ResponseData{
    data:{
        levelDictList:LevelDict[]
    }
}