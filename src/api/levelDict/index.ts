import request from "@/utils/request.ts";
import {LevelDictListResponseData} from "@/api/levelDict/type.ts";

enum API{
    GET_LEVEL_DICT_LIST='/core/levelDict/getLevelDictList'
}

export const getLevelDictList = ()=>
    request.get<any,LevelDictListResponseData>(API.GET_LEVEL_DICT_LIST)