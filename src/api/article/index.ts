import request from "@/utils/request.ts";
import {
    Article,
    ArticleDetailResponseData,
    ArticlesPageResponseData,
    ArticlesResponseData,
    CommentResponseData, GetArticleByUserListResponseData,
    GetArticleResponseData,
    GetCommentsResponseData,
    GetMessageResponseData,
    GetPartNameResponseData,
    GetRewardListResponseData,
    HomeArticleListResponseData,
    ResponseData,
    SubArticle
} from "@/api/article/type.ts";
import {GetDirectionListResponseData} from "@/api/user/type.ts";

enum API{
    //获取首页显示的文章列表
    GET_HOME_ARTICLE_LIST = '/core/article/getHomeRecommendList/',
    //发布帖子
    ADD_ARTICLE_URL = '/core/article/addArticle/',
    //根据用户ID查找文章
    GET_ARTICLE_BY_ID='/core/article/articleDetail/',
    //更新文章的点赞状态
    UPDATE_ARTICLE_LIKE='/core/article/updateLike/',
    //添加评论
    ADD_COMMENT_URL = '/core/comment/addComment/',
    //获取评论列表
    GET_COMMENTS_URL='/core/comment/getComment/',
    //获取文章列表
    GET_ARTICLES_URL='/core/article/getArticles/',
    //根据搜索获取文章列表
    GET_ARTICLES_BY_SEARCH='/core/article/getArticlesBySearch/',
    //根据文章ID获取文章详细信息
    GET_ARTICLE_BY_ARID='/core/article/getArticleByArId/',
    //删除
    DELETE_ARTICLE_BY_ID='/core/article/deleteArticle/',
    //获取文章作者信息
    GET_MESSAGE_BY_USERID='/core/article/getMessageById/',
    //获取分区名
    GET_PART_NAME_BY_ID='/core/part/getPartName/',
    //删除评论
    DELETE_COMMENT='/core/comment/deleteComment/',
    //获取打赏列表
    GET_REWARD_LIST='/core/reward/getRewardList/',
    //打赏文章
    ADD_REWARD='/core/reward/addReward/',
    //新增收藏信息
    ADD_COLLECTION = '/core/collection/addCollection/',
    //删除收藏信息
    REMOVE_COLLECTION = '/core/collection/removeCollection/',
    //获取收藏目录表
    GET_DIRECTION_LIST='/core/collection/getDirectionList',
    //获取用户的已通过审核的文章列表
    GET_ARTICLE_BY_USER='/core/article/getArticleByUser'

}
export const getHomeArticleList = ()=>
    request.get<any,HomeArticleListResponseData>(API.GET_HOME_ARTICLE_LIST)
//增加一篇文章
export const addArticle = (data:Article|SubArticle,flag:number,token:string)=>
    request.post<any,ArticleDetailResponseData>(API.ADD_ARTICLE_URL+flag,data,{
        headers:{
            token:token
        }
    })

export const getArticle = (id:string,token:string)=>
    request.get<any,ArticleDetailResponseData>(API.GET_ARTICLE_BY_ID+id,{
        headers:{
            token:token
        }
    })
//更新文章的点赞数
export const updateArticleLike = (arId:string,flag:number,token:string)=>
    request.get<any,any>(API.UPDATE_ARTICLE_LIKE+`${arId}/${flag}`,{
        headers:{
            token:token
        }
    })
//增加一条评论
export const addComment = (arId:string,comParentId:string,token:string,content:string,comId:string)=>
    request.post<any,CommentResponseData>(API.ADD_COMMENT_URL+`${arId}/${comParentId}/${comId}?content=${content}`,{
    },{
        headers:{
            token:token
        }
    })

//分页获取所有评论
export const getComments = (arId:string,pageNo:number,pageSize:number,userId:number)=>
    request.get<any,GetCommentsResponseData>(API.GET_COMMENTS_URL+`${arId}/${pageNo}/${pageSize}/${userId}`)

//分页获取所有的文章
export const getArticles = (pageNo:number,pageSize:number,partId:number,childId:number,sortFlag:number)=>
    request.get<any,ArticlesPageResponseData>(API.GET_ARTICLES_URL+`/${pageNo}/${pageSize}/${partId}/${childId}/${sortFlag}`)
//搜索
export const getArticlesBySearch = (search:string) => request.get<any,ArticlesResponseData>(API.GET_ARTICLES_BY_SEARCH+`${search}`)

//根据id获取文章信息---用户或管理员编辑更新
export const getArticleById = (arId:string,token:string)=> request.get<any,GetArticleResponseData>(API.GET_ARTICLE_BY_ARID+`${arId}`,{
    headers:{
        token:token
    }
})
//删除文章
export const deleteArticleById = (arId:string,token:string)=> request.post<any,ResponseData>(API.DELETE_ARTICLE_BY_ID+`${arId}`,{},{
    headers:{
        token:token
    }
})
//根据用户id获取信息--应用于个人中心
export const getMessageByUserId = (userId:number,token:string)=> request.get<any,GetMessageResponseData>(API.GET_MESSAGE_BY_USERID+`${userId}`,{
    headers:{
        token:token
    }
})
//根据id获取分区名
export const getPartNameById = (id:number)=> request.get<any,GetPartNameResponseData>(API.GET_PART_NAME_BY_ID+`${id}`)

//删除评论
export const deleteComment = (commentId:string,token:string)=> request.post<any,ResponseData>(API.DELETE_COMMENT+`${commentId}`,{},{
    headers:{
        token:token
    }
})

export const getRewardList = (arId:string,flag:number)=> request.get<any,GetRewardListResponseData>(API.GET_REWARD_LIST+`${arId}/${flag}`)

export const addReward = (arId:string,gold:number,diamond:number,message:string,token:string)=>
    request.post<any,ResponseData>(API.ADD_REWARD+`${arId}/${gold}/${diamond}?message=${message}`,'',{
        headers:{
            token:token
        }
    })
export const addCollection = (arId:string,direction:string,token:string)=>
    request.post<any,ResponseData>(API.ADD_COLLECTION+`${arId}?direction=${direction}`,'',{
        headers:{
            token:token
        }
    })

export const removeCollection = (arId:string,token:string)=>
    request.post<any,ResponseData>(API.REMOVE_COLLECTION+`${arId}`,'',{
        headers:{
            token:token
        }
    })
export const getDirectionList = (token:string)=> request.get<any,GetDirectionListResponseData>(API.GET_DIRECTION_LIST,{
    headers:{
        token:token
    }
})

export const getArticleByUser = (token:string)=> request.get<any,GetArticleByUserListResponseData>(API.GET_ARTICLE_BY_USER,{
    headers:{
        token:token
    }
})

