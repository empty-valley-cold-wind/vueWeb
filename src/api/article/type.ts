import {LoginData, UserInfo} from "@/api/user/type.ts";

export interface ResponseData{
    code:number,
    message:string,
}
//分区的数据类型
export interface Article{
    arId:string
    partId:number|string,
    parentId:number|string,
    userId:number,
    username:string,
    title:string,
    content:string,
    level:number,
    createTime:string,
    updateTime:string,
    likeNum:number,//点赞数
    comNum:number,//评论数
    click:number,
    coverAddress:string,//封面地址,
    description:string,//简要描述
    isReprint:number,//是否转载
    originalAddress:string,//原帖地址
    isVetting:number//审核状态
}
export interface HomeArticle{
    partId:number,
    partName:string,
    articleList:Article[]
}
//提交时的article
export interface SubArticle{
    arId:string,
    partId:string,
    parentId:string,
    userId:number,
    title:string,
    content:string,
    coverAddress:string,
    description:string,
    isReprint:number,//是否转载
    originalAddress:string,//原帖地址
}
export interface Comment{
    comId:string,
    userId:number,
    content:string,
    createTime:string,
    likeNum:number,
    ComParentId:number,
    user:UserInfo,
    preUserId:number,
    preUsername:string,
    preContent:string,
}
export interface Reward{
    rewardId:number,
    giverId:number,
    arId:string,
    gold:number,
    diamond:number,
    message:string
}
export interface RewardVo{
    reward:Reward,
    username:string
}
export interface ArticleDetail extends Article{
    photoAddress:string,
    like:boolean,//是否点过赞
    collect:boolean,//是否已经收藏
    collectionNumber:number,//收藏数
    jurisdiction:number,
    partName:string,
    partChildName:string
}
//
export type Content=ArticleDetail[]
export interface ArticlesPageResponseData extends ResponseData{
    data:{
        articles:{
            records:Content,
            current:number,
            size:number,
            total:number,
        }
    }
}
export interface HomeArticleListResponseData extends ResponseData{
    data:{
        homeArticleList:HomeArticle[]
    }
}
export interface ArticlesResponseData extends ResponseData{
    data:{
        articles: Content,

    }
}
export interface ArticleDetailResponseData extends ResponseData{
    data:{
      article:ArticleDetail,
        userInfo:UserInfo
    }
}
export interface CommentResponseData extends ResponseData{
    data:{
        commentDetail:Comment,
        userInfo:LoginData
    }
}
export interface GetCommentsResponseData extends ResponseData{
    data:{
        commentsPage:{
            current:number,
            size:number,
            total:number,
            records:Comment[]
        }
    }
}

export interface GetArticleResponseData extends ResponseData{
    data:{
        article:Article
    }
}

export interface GetMessageResponseData extends ResponseData{
    data:{
        articles:Content,
        user:UserInfo
    }
}

export interface GetPartNameResponseData extends ResponseData{
    data:{
        partName:string
    }
}
export interface GetRewardListResponseData extends ResponseData{
    data:{
        rewardList:RewardVo[]
    }
}
export interface GetArticleByUserListResponseData extends ResponseData{
    data:{
        articleList:Article[]
    }
}

