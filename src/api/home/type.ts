//首页模块的数据类型
import Part from "@/pages/home/part/index.vue";

export interface ResponseData{
    code:number,
    message:string,
}
//分区的数据类型
export interface Part{
    partId:number,
    parentId:number,
    partName:string,
    value:number
}
export interface RecommendDetail{
    arId:number,
    coverAddress:string
}
//
export type Content=Part[]
export interface PartsResponseData extends ResponseData{
    data:{
        parts:Content
    }
}
export interface RecommendDetailListResponseData extends ResponseData{
    data:{
        recommendList:RecommendDetail[]
    }
}

//每日签到返回值
export interface SignResponseData extends ResponseData{
    data:{
        continuousSignNumber:number
    }
}