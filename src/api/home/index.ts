//统一管理首页模块接口
import request from '@/utils/request'
import {PartsResponseData, RecommendDetailListResponseData, ResponseData, SignResponseData} from "@/api/home/type.ts";



//通过枚举管理首页模块的接口地址
enum API{
    //获取父分区
    PARTS_URL = '/core/part/getParts/',
    //获取子分区
    PART_CHILDREN_URL='/core/part/getPartChildren/',
    //获取推荐
    GET_RECOMMEND_DETAIL='/core/article/getRecommendList',
    //移除目录
    REMOVE_FILE='/oss/file/remove/',
    //验证今日是否签到
    VERIFY_SIGN='/core/sign/verifyDailySign',
    //每日签到
    DAILY_SIGN='/core/sign/dailySign'
}
//获取父分区
export const reqParts = ()=>
    request.get<any,PartsResponseData>(API.PARTS_URL)
export const reqPartChildren = (parentId:number)=>
    request.get<any,PartsResponseData>(API.PART_CHILDREN_URL+`${parentId}`)

export const getRecommendDetails = ()=>
    request.get<any,RecommendDetailListResponseData>(API.GET_RECOMMEND_DETAIL)

export const removeFile = (flag:number,id:string)=>
    request.get<any,ResponseData>(API.REMOVE_FILE+`${flag}/${id}`)

//验证今日是否已经签到
export const verifySign = (token:string)=>
    request.get<any,SignResponseData>(API.VERIFY_SIGN,{
        headers:{
            token:token
        }
    })
//每日签到
export const dailySign = (token:string)=>
    request.post<any,SignResponseData>(API.DAILY_SIGN,'',{
        headers:{
            token:token
        }
    })
