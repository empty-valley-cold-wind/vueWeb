export interface ResponseData{
    code:number,
    message:string,
}
export interface Task{
    taskId:number,
    taskContent:string,
    taskTitle:string,
    taskType:number,
    gold:number,
    experience:number,
    diamond:number
}
export interface TaskVo{
    task:Task,
    isCompleted:number,//是否已完成---对于每日任务和一次性任务
    CompleteCount:number//完成次数---对于每日任务和长期任务
}
export interface TaskListResponseData extends ResponseData{
    data:{
        taskList:TaskVo[]
    }
}
