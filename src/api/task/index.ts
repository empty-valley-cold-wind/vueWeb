import request from "@/utils/request.ts";
import {TaskListResponseData} from "@/api/task/type.ts";

enum API{
    GET_TASK_LIST='/core/task/getTaskList'
}

export const getTaskList = (token:string)=>
    request.get<any,TaskListResponseData>(API.GET_TASK_LIST,{
        headers:{
            token:token
        }
    })