export interface ResponseData{
    code:number,
    message:string,
}

//用户上传接口返回的参数
export interface UploadData{
    url:string
}
export interface UploadResponseData extends ResponseData{
    data: { url: string }
}