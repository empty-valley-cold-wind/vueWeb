import request from "@/utils/request.ts";
import {
    NoticeDetail,
    NoticeListResponseData,
    NoticeNumberResponseData,
    ResponseData
} from "@/api/notice/type.ts";

enum API {
    GET_NOTICE='/core/notice/getNotice/',
    GET_NOTICE_NUMBER='/core/notice/getNoticeNum/',
    UPDATE_NOTICE='/core/notice/updateNotice',
    DELETE_NOTICE='/core/notice/deleteNotice'
}

export const getNoticeList = (token:string)=>
    request.get<any,NoticeListResponseData>(API.GET_NOTICE,{
        headers:{
            token:token
        }
    })

export const getNoticeNumber = (token:string)=>
    request.get<any,NoticeNumberResponseData>(API.GET_NOTICE_NUMBER,{
        headers:{
            token:token
        }
    })

export const updateNotice = (list:NoticeDetail[],token:string)=>
    request.post<any,ResponseData>(API.UPDATE_NOTICE,list,{
        headers:{
            token:token
        }
    })

export const deleteNotice = (list:NoticeDetail[],token:string)=>
    request.post<any,ResponseData>(API.DELETE_NOTICE,list,{
        headers:{
            token:token
        }
    })
