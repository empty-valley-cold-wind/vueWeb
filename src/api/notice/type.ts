export interface ResponseData{
    code:number,
    message:string,
}

export interface NoticeDetail{
    noticeId:number,
    username:string,
    associateId:number,//关联用户id
    arId:string,//关联文章ID
    noticeMessage:string,
    noticeType:number,
    userId:number,
    noticeTime:string,
    isReading:number,//是否已读
    scroll:number,//滚动定位
    pageNo:number,//页面定位
}

export interface NoticeListResponseData extends ResponseData{
    data:{
        noticeList:NoticeDetail[]
    }
}

export interface NoticeNumberResponseData extends ResponseData{
    data:{
        number:number
    }
}