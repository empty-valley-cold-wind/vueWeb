import request from "@/utils/request.ts";
import {HomeArticleListResponseData} from "@/api/article/type.ts";
import {ResponseData} from "@/api/event/type.ts";

enum API{
    //提交事件
    ADD_EVENT='/core/event/addEvent/'
}

export const addEvent = (type:number,id:string,message:string,token:string)=>
    request.post<any,ResponseData>(API.ADD_EVENT+`${type}/${id}?message=${message}`,'',{
        headers:{
            token:token
        }
    })