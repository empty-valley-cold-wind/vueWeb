
import { createApp } from 'vue'
//引入跟组件
import App from '@/App.vue';
//引入pinia仓库
import pinia from '@/store/index.ts'
//引入reset
import '@/style/reset.scss'

//引入全局组件
import WebTop from '@/components/web_top/index.vue';
//引入vuerouter
import router from '@/router'
//引入element-plus
import ElementPlus from 'element-plus'
import zhCn from 'element-plus/dist/locale/zh-cn.mjs';

import Login from '@/components/login/index.vue'

import 'element-plus/dist/index.css'
//创建应用实例并挂载
const app = createApp(App)
app.component('WebTop',WebTop)
app.component('Login',Login)
app.use(router)
app.use(ElementPlus, {
    locale: zhCn,
})
app.use(pinia)
app.mount('#app')
